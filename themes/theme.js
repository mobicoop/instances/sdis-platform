export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#072778',
      accent: '#EF5350',
      secondary: '#D50000',
      success: '#8ec760',
      info: '#CB1414',
      warning: '#ffaa00',
      error: '#F03C0E'
    },
    dark: {
      primary: '#072778',
      accent: '#EF5350',
      secondary: '#D50000',
      success: '#8ec760',
      info: '#CB1414',
      warning: '#ffaa00',
      error: '#F03C0E'
    }
  }
}
