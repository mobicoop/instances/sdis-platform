

blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)sdis-platform ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)sdis-platform ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)sdis-platform ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info sdis-platform ($(os)): Starting sdis-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info sdis-platform ($(os)): Stopping sdis-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep sdis-platform_platform
	@docker ps -a | grep sdis-platform_db
 
restart:
	$(info sdis-platform ($(os)): Restarting sdis-platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting sdis-platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info sdis-platform ($(os)): Stopping sdis-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)sdis-platform+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f sdis-platform

go-platform:
	@docker exec -it sdis-platform zsh